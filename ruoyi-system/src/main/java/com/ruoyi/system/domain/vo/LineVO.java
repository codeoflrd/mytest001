package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

/**
 * @version 1.0
 * @author: LiRendong
 * @date: 2021/6/28 10:47
 */
@Data
public class LineVO {
    private String[] xAxis = {"a","b"};
    private List<Integer> date;
    private String name;


}
