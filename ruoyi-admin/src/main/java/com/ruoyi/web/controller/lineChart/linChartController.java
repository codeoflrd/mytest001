package com.ruoyi.web.controller.lineChart;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.vo.LineVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @version 1.0
 * @author: LiRendong
 * @date: 2021/6/28 10:44
 */
@Controller
@RequestMapping("/line")
public class linChartController extends BaseController {

    public static final String[] CHINESEMONTHS = {"一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"};

    @RequestMapping("/lineX")
    public AjaxResult line() {
        List<Integer> date1 = new ArrayList<>();
        //制造数据1
        for (int i = 0; i <= 12; i++) {
            date1.add(i);
        }
        //制造数据2
        List<Integer> date2 = new ArrayList<>();
        for (int i = 12; i <= 24; i++) {
            date2.add(i);
        }

        List<Integer> a = new ArrayList<>();

        LineVO lineVO1 = new LineVO();
        Map<String, Object> map = new HashMap<>();
        map.put("months", CHINESEMONTHS);
        map.put("xAxis", lineVO1.getXAxis());

        //数据1
        lineVO1.setName(lineVO1.getXAxis()[0]);
        for (Integer i1 : date1) {
            a.add(i1);
        }

        //数据2
        LineVO lineVO2 = new LineVO();
        lineVO1.setName(lineVO1.getXAxis()[1]);
        List<Integer> b = new ArrayList<>();
        for (Integer i2 : date1) {
            b.add(i2);
        }

        //数据放入结果
        List<LineVO> result = new ArrayList<>();
        result.add(lineVO1);
        result.add(lineVO2);
        map.put("result", result);

        return AjaxResult.success(map);
    }


    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(i);
        }
        list = list.stream().filter(t -> 0 != t % 2).collect(Collectors.toList());

        System.out.println("输出list过滤结果" + list);
    }

}
